#!/usr/bin/env node
'use strict';
import camelizeKeys from 'camelcase-keys';
import { parse } from 'content-disposition';
import download = require('download');
import { writeFileSync } from 'fs';
import { resolve as resolvePath } from 'path';

(async () => {
  let dl: NodeJS.ReadableStream = <any>undefined;
  const [, , ...targetUris] = process.argv;

  for (const targetUri of targetUris) {
    const dpTask = new Promise<string>((resolve, reject) => {
      dl = download(targetUri)
        .on('response', ({ headers }) => {
          const { contentDisposition: disposition } = <any>camelizeKeys(headers);
          const { filename } = disposition && parse(disposition).parameters || <any>{};
          resolve(filename || targetUri.split('/').pop());
        })
        .on('error', e => reject(e));
    });

    const dp = resolvePath(await dpTask);

    if (dl) {
      writeFileSync(dp, await dl);
      console.log(`Content from '${targetUri}' has been saved to '${dp}'.`);
    }
  }
})();
